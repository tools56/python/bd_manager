class Glob:
    """Espace de noms pour les variables et fonctions <pseudo-globales>"""
    connect={"dbName":"Optim",    # nom de la base de données
             "user"  :"root" ,    # utilisateur
             "passwd":"root" ,    # mot de passe d'accès
             "host"  :"localhost"}# nom ou adresse IP du serveur
    
    # Structure de la base de données.  Dictionnaire des tables & champs :
    dicoT ={"compositeurs":[('id_comp', "k", "clé primaire"),
                            ('nom', 25, "nom"),
                            ('prenom', 25, "prénom"),
                            ('a_naiss', "i", "année de naissance"),
                            ('a_mort', "i", "année de mort")],
            "oeuvres":[('id_oeuv', "k", "clé primaire"),
                       ('id_comp', "i", "clé compositeur"),
                       ('titre', 50, "titre de l'oeuvre"),
                       ('duree', "i", "durée (en minutes)"),
                       ('interpr', 30, "interprète principal")]}