"""
   gest_bd.py
   :author: Stephane Hamel
"""
import sys
from mysql import Mysqlmanager
from dict_bd import Glob

class Enregistreur:
    """
       Class Enregistreur:
       Gére l'entrée d'enregistrements divers
    """
    def __init__(self, database, table):
        """
           Function Constructeur
           :param database: TODO
           :type database: TODO
           :param table: TODO
           :type table: TODO
        """
        self.database =database
        self.table =table
        self.descriptif =Glob.dicoT[table]   # descriptif des champs

    def entrer(self):
        """
           Function entrer
           procédure d'entrée d'un enregistrement entier
           :returns: Indique si l'utilisateur veut continuer a entre des entrees
           :rtype: boolean
        """
        dico_champs={}
        # Demander successivement une valeur pour chaque champ :
        for cha, tcha, nom in self.descriptif:
            if tcha =="k": # on ne demandera pas le n° d'enregistrement
                continue   # à l'utilisateur (numérotation auto.)

            loop=True
            while loop:
                loop=False
                val = input("Entrez le champ %s :" % nom)
                if tcha=="i":
                    if not val.isdigit():
                        print("Erreur veuilllez rentrer un nombre")
                        loop=True

            if tcha =="i":
                dico_champs[cha]=val
            else:
                dico_champs[cha]="'%s'"%(val)

        self.database.insert(self.table, dico_champs)

        loop=False
        reponse =input("Continuer (O/N) ? ")
        if reponse.upper() == "O":
            loop=True
        return loop

def op_table(database):
    """
       op_table
       :param database: connection à la base
       :type database: class de gestion de bd
    """
    print("\nQue voulez-vous faire :\n"\
            "1) Créer les tables de la base de données"\
            "2) Supprimer les tables de la base de données ?"\
            "3) Effacer le contenu des tables de la base de données ?"\
            "4) Lister les tables"\
            "\n")
    reponse = int(input("Votre choix :"))
    if reponse ==1:
        # création de toutes les tables décrites dans le dictionnaire :
        database.creer_tables(Glob.dicoT)
    elif reponse ==2:
        # suppression de toutes les tables décrites dans le dic. :
        for key in Glob.dicoT:
            database.supprimer_table(key)
    elif reponse ==3:
        # suppression de toutes les tables décrites dans le dic. :
        for key in Glob.dicoT:
            database.effacer_table(key)
    elif reponse ==4:
        # Liste toutes les tables :
        for table in database.get_tables():
            print("Table :"+table)
    else:
        print("Choix incorrect")

def op_list(database):
    """
       op_list
       :param database: connection à la base
       :type database: class de gestion de bd
    """
    print("\nQuelle table voulez-vous :\n")
    lst_tables=database.get_tables()
    reponse=print_menu(lst_tables)
    if 1 <= reponse <= len(lst_tables):
        records=database.get_records(lst_tables[reponse-1])
        if records is not None:
            for rec in records: # => chaque enregistrement
                for item in rec:# => chaque champ
                    print(item)
                print("")

def op_in(database):
    """
       op_in
       :param database: connection à la base
       :type database: class de gestion de bd
    """
    #on ne peut entrer des valeurs que dans les tables du dico
    lst_tables=[]
    for key in Glob.dicoT:
        lst_tables.append(key)

    print("\nDans quel table voulez vous enter des valeurs :\n")
    reponse=print_menu(lst_tables)
    if 1 <= reponse <= len(lst_tables):
        print("enreg")
        # création d'un <enregistreur> :
        enreg =Enregistreur(database, lst_tables[reponse-1])
        while enreg.entrer():
            pass

def print_menu(lst):
    """
       print_menu
       :param lst: liste du menu
       :type lst: liste
    """
    ind=0
    for table in lst:
        ind+=1
        print(str(ind)+")"+table)
    print(str(ind+1)+")Retour\n")
    reponse = int(input("Votre choix :"))
    return reponse
    
def main():
    """
        Main
    """
    # Création de l'objet-interface avec la base de données :
    database = Mysqlmanager(Glob.connect)
    if database.echec:
        print(database.mess)
        sys.exit()

    bool_exit=False
    while not bool_exit:
        print("\nQue voulez-vous faire :\n"\
            "1) Operation sur les tables\n"\
            "2) Entrer des enregistrements\n"\
            "3) Lister une table\n"\
            "4) Exécuter une requête SQL quelconque\n"\
            "5) Sauver et Quitter\n"\
            "6) Quitter sans sauver\n")
        reponse = int(input("Votre choix :"))
        if reponse ==1:
            op_table(database)
        elif reponse == 2:
            op_in(database)
        elif reponse == 3:
            op_list(database)
        elif reponse ==4:
            req =input("Entrez la requête SQL : ")
            if database.executer_req(req):
                print(database.resultat_req())# ce sera un tuple de tuples
        elif reponse in [5,6]:
            if reponse ==5:
                database.commit()
            database.close()
            bool_exit=True

if __name__ == '__main__':
    main()
