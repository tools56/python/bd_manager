"""
    Gestionnaire de basse de donnees MySQL
"""
import MySQLdb

class Mysqlmanager:
    """
        Class Mysqlmanager :
        Mise en place et interfaçage d'une base de données MySQL
    """
    def __init__(self, connect):
        """
           Function Constructeur
           :param connect: Parametres de connection
           :type connect: dico avec les cles dbName,user,passwd,host,port
        """
        try:
            if not 'port' in connect :
                connect['port']=3306
            self.db_name=connect['dbName']
            self.base = MySQLdb.connect(db =connect['dbName'],
            user =connect['user'],
            passwd =connect['passwd'],
            host =connect['host'],
            port =connect['port'])
        except Exception as err:
            self.mess='La connexion avec la base de données a échoué :\n'\
                'Erreur détectée :\n%s'%err
            self.echec =True
        else:
            self.cursor = self.base.cursor()   # création du curseur
            self.mess=""
            self.echec =False

    def creer_tables(self, dic_tables):
        """
           Function creer_tables
           Création des tables décrites dans le dictionnaire <dic_tables>."
           :param dic_tables: Descriptif des tables
           :type dic_tables: dictionnaire
        """
        for table in dic_tables:# parcours des clés du dict.
            req = "CREATE TABLE %s (" % table
            primary_key =''
            for descr in dic_tables[table]:
                nom_champ = descr[0] # libellé du champ à créer
                tch = descr[1] # type de champ à créer
                if tch =='i':
                    type_champ ='INTEGER'
                elif tch =='k':
                    # champ 'clé primaire' (incrémenté automatiquement)
                    type_champ ='INTEGER AUTO_INCREMENT'
                    primary_key = nom_champ
                else:
                    type_champ ='VARCHAR(%s)' % tch
                req = req + "%s %s, " % (nom_champ, type_champ)
            if primary_key == '':
                req = req[:-2] + ")"
            else:
                req += "CONSTRAINT %s_primary_key PRIMARY KEY(%s))" % (primary_key, primary_key)
            self.executer_req(req)
    def get_tables(self):
        """
           Function get_tables
           Retournes les tables presentes dans la base de données
           :return: nom des tables
           :rtype: liste
        """
        req="SELECT TABLE_NAME "\
            " FROM INFORMATION_SCHEMA.TABLES "\
            " WHERE TABLE_TYPE = 'BASE TABLE' "\
            " AND TABLE_SCHEMA='"+self.db_name+"'"
        lst=[]
        if self.executer_req(req):
            records=self.resultat_req()
            for rec in records: # => chaque enregistrement
                for item in rec:# => chaque champ dans l'enreg.
                    lst.append(item)
        return lst

    def supprimer_table(self, table):
        """
           Function supprimer_table
           Suppression de table
           :param table: nom de la table
           :type table: string
        """
        req ="DROP TABLE %s" % table
        self.executer_req(req)
        self.commit() # transfert -> disque
    
    def renommer_table(self, name_table,new_name_table):
        """
           Function renommer_table
           renommer une table
           :param table: nom de la table
           :type table: string
        """
        req ="ALTER TABLE %s rename to %s" %(name_table,new_name_table)
        self.executer_req(req)
        self.commit() # transfert -> disque

    def effacer_table(self, table):
        """
           Function effacer_table
           Efface tout le contenu de table
           :param table: nom de la table
           :type table: string
        """
        req ="DELETE FROM %s" % table
        self.executer_req(req)
        self.commit() # transfert -> disque

    def executer_req(self, req):
        """
           Function executer_req
           Exécution de la requête <req>, avec détection d'erreur éventuelle"
           :param req: requete
           :type req: string
           :returns: 0= Erreur 1=Ok
           :rtype: Integer
        """
        try:
            self.cursor.execute(req)
        except Exception as err:
            # afficher la requête et le message d'erreur système :
            print("Requête SQL incorrecte :\n%s\nErreur détectée :\n%s"\
                % (req, err))
            return 0
        else:
            return 1

    def resultat_req(self):
        """
           Function resultat_req
           renvoie le résultat de la requête précédente (un tuple de tuples)"
           :returns: resultat de la requete
           :rtype: Tous les enregistrements du resultat de la requete
        """
        return self.cursor.fetchall()

    def insert(self,table,dico_champs):
        """
           Function insert
           :param table: nom de la table
           :type table: string
           :param dico_champs: nom du champ=valeur
           :type dico_champs: dictionnaire
        """
        champs='('
        valeurs='('
        sep=""
        for cle, valeur in dico_champs.items():
            champs+=sep+cle
            valeurs+=sep+valeur
            sep=","
        champs+=")"
        valeurs+=")"

        req ="INSERT INTO %s %s VALUES %s" % (table, champs, valeurs)
        self.executer_req(req)

    def get_records(self,table):
        """
           Function get_records
           :param table: nom de la table
           :type table: string
           :returns: Tous les enregistrements de la table
           :rtype: records
        """
        records=None
        if self.executer_req("SELECT * FROM %s" % table):
            records = self.resultat_req()      # ce sera un tuple de tuples
        return records

    def commit(self):
        """
           Function commit
        """
        if self.base:
            self.base.commit() # transfert curseur -> disque

    def close(self):
        """
           Function close
        """
        if self.base:
            self.base.close()
